//
//  DLVisitorTableViewController.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/12/1.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit

class DLVisitorTableViewController: UITableViewController {
    
    
    var isLogin: Bool = false
    
    var visitorView: DLVisitorView?
    
    //  自定义视图是需要重写  loadView方法
    override func loadView() {
        if isLogin == false{
            visitorView = DLVisitorView()
            view = visitorView
            
            visitorView?.callBack = { [weak self] in
                print(self ?? "")
                print("我是闭包调用过来的")
                self?.requestOAuth()
            }
            setupUI()
        }else{
            super.loadView()
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    private func setupUI() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "注册", target: self, action: #selector(registerButtonAction))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "登录", target: self, action: #selector(loginButtonAction))
        
    }
    @objc private func registerButtonAction() {
        print("注册")
        requestOAuth()
    }
    @objc private func loginButtonAction() {
        print("登入")
        requestOAuth()
    }
    
    //第三方登录的
    private func requestOAuth() {
        let OAuth: DLOAuthViewController = DLOAuthViewController()
        let nav = DLMainNavgationController(rootViewController: OAuth)
        present(nav, animated: true, completion: nil)
    }
    
    
    
    
}
