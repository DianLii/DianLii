//
//  DLVisitorView.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/12/1.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit
import SnapKit

class DLVisitorView: UIView {
    
    
    
    // 定义点击按钮闭包
    var callBack: (()->())?
    
    //MARK: - 视图 - 懒加载：
    private lazy var rotationImageView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    private lazy var maskImageView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    private lazy var iconImageView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    
    //  消息lable
    private lazy var messagelable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont.systemFont(ofSize: 13)
        lable.text = "关注一些人，回这里看看有什么惊喜"
        lable.numberOfLines = 0
        lable.textColor = UIColor.darkGray
        lable.textAlignment = .center
        
        return lable
    }()
    
    //注册按钮
    private lazy var registerButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        button.setTitle("注册", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.setTitleColor(UIColor.orange, for: .highlighted)
        button.setBackgroundImage(UIImage(named: "common_button_white_disable"), for: .normal)
        button.adjustsImageWhenHighlighted = false
        return button
    }()
    // 登录按钮
    private lazy var loginButton: UIButton = {
       let button = UIButton()
        button.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        button.setTitle("登录", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.setTitleColor(UIColor.orange, for: .highlighted)
        button.setBackgroundImage(UIImage(named: "common_button_white_disable"), for: .normal)
        button.adjustsImageWhenHighlighted = false
        return button
    }()
    
    //MARK: 点击按钮实现的方法
    func clickButton() {
        callBack?()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.backgroundColor = UIColor(white: 237/255, alpha: 1)
        
        addSubview(rotationImageView)
//        rotationImageView.sizeToFit()
        addSubview(maskImageView)
        addSubview(iconImageView)
        addSubview(messagelable)
        addSubview(registerButton)
        addSubview(loginButton)
        
        rotationImageView.snp.makeConstraints { (make) in
            make.center.equalTo(self)
        }
        maskImageView.snp.makeConstraints { (make) in
            make.center.equalTo(rotationImageView)
        }
        iconImageView.snp.makeConstraints { (make) in
            make.center.equalTo(rotationImageView)
        }
        messagelable.snp.makeConstraints { (make) in
            make.top.equalTo(rotationImageView.snp.bottom)
            make.centerX.equalTo(rotationImageView)
            make.width.equalTo(224)
        }
        registerButton.snp.makeConstraints { (make) in
            make.left.equalTo(messagelable)
            make.top.equalTo(messagelable.snp.bottom).offset(10)
            make.size.equalTo(CGSize(width: 80, height: 35))
        }
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(registerButton)
            make.size.equalTo(registerButton)
            make.right.equalTo(messagelable)
        }
        
    }
    
    // 动画开启
    private func startAnimation() {
        //创建核心动画
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        //动画时长
        animation.duration = 10
        //旋转角度
        animation.toValue = 2 * M_PI
        // 执行动画次数
        animation.repeatCount = MAXFLOAT
        // 失去焦点不让其释放对象
        animation.isRemovedOnCompletion = false
        //添加动画  执行
        rotationImageView.layer.add(animation, forKey: nil)
        
    }
    
    func updateVisiterInfo(image: String?, Info: String?) {
        guard let _ = image, let _ = Info else {
            startAnimation()
            return
        }
        
//        来到此处说明传过来的 图片和信息都是有值的
        iconImageView.image = UIImage(named: image!)
        messagelable.text = Info
        rotationImageView.isHidden = true
        
    }
    



}
