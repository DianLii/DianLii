//
//  DLTabBar.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/11/30.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit

class DLTabBar: UITabBar {

    
    // 设置闭包
    var callBack: (()->())?
    
    
    private lazy var composeButton: UIButton = {
        let button = UIButton()
        // 添加按钮点击事件
        button.addTarget(self, action: #selector(composeButtonAction), for: .touchUpInside)
        //      创建按钮北京图片（橘色图片）
        button.setBackgroundImage(UIImage(named: "tabbar_compose_button"), for: .normal)
        button.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), for: .highlighted)
        //      创建按钮图片  （加号）
        button.setImage(UIImage(named: "tabbar_compose_icon_add"), for: .normal)
        button.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), for: .highlighted)
        button.sizeToFit()
        
        return button
    }()
    
     override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
//    加载stroboard和xib的时候会调用
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupUI() {
        self.backgroundImage = UIImage(named: "tabbar_background")
        addSubview(composeButton)
    }
    
//    MARK:  点击按钮实现的方法
    //  使用private 修饰的事件函数在swift的运行循环里面是找不到, 因为swift是编译型语言,编译的是就需要知道执行那个函数,为了追求性能
    //  我们需要使用@objc  , 告诉编译器我们使用oc的方式去调用这个方法, oc是基于运行时,使用kvc方式动态派发

    @objc private func composeButtonAction() {
        callBack?()
    }
    
    
//    MARK: - 计算tabbar布局  调用LayoutSubView
    override func layoutSubviews() {
        super.layoutSubviews()
        //  设置button的中心点      分类声明了 UIview的宽、高、x、y、centerX、centerY
        composeButton.center.x = width / 2
        composeButton.center.y = height / 2
        //        composeButton.center = self.center  这个不行  Y轴是642？？？？？
        
        let buttonWidth = width / 5
        var index: CGFloat = 0
        
        for childItem in subviews {
            // 判断是否是UItableBarButton
            if childItem.isKind(of: NSClassFromString("UITabBarButton")!) {
            
                childItem.width = buttonWidth
                childItem.x = index * buttonWidth
                index += 1
                //  在第2个item的时候会覆盖 所以到这里时在加一个 1
                if index == 2 {
                    index += 1
                }
                
            }
        }
        
        
    }
    

}
