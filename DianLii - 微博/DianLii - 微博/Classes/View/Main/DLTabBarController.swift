//
//  DLTabBarController.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/11/30.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit

class DLTabBarController: UITabBarController {

    
    
    
    var back = {() in
        print("aa")
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        设置自定义tabBar
        let dltabBar = DLTabBar()
//        tabBar的闭包实现   这里如果输出self 会出现 循环引用   
        /*
         自定义tabbar ->拥有 callBack闭包  tabbarController拥有自定义tabbar   闭包输出tabarController  导致循环引用问题  
         */
        dltabBar.callBack = { [weak self] in
            print(self ?? "加号没过来")
        }
//        self.tabBar = DLTabBar   因为tabbar是只读属性  所以这种方式复制会报错
        setValue(dltabBar, forKey: "tabBar")
        
        
        
        addChildViewController(childController: DLHomeTableViewController(), imageName: "tabbar_home", title: "首页")
        addChildViewController(childController: DLMessageTableViewController(), imageName: "tabbar_message_center", title: "消息")
        addChildViewController(childController: DLDiscoverTableViewController(), imageName: "tabbar_discover", title: "发现")
        addChildViewController(childController: DLProfileTableViewController(), imageName: "tabbar_profile", title: "我")
        
    }

//     重载添加子视图控制器
    func addChildViewController(childController: UIViewController, imageName: String, title: String) {
//        设置tabbarItem图片
        childController.tabBarItem.image = UIImage(named: imageName)
        
//        设置选中图片   修改图片选人方式
        childController.tabBarItem.selectedImage = UIImage(named: imageName + "_selected")?.withRenderingMode(.alwaysOriginal)
        
//        设置文字选中颜色
        childController.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.orange], for: .selected)
        
//        设置文字大小
        childController.tabBarItem.setTitleTextAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 11)], for: .normal)
        
//        Vc.tabBarItem.title = "首页"
//        Vc.navigationItem.title = "首页"
//        设置标题
        childController.title = title
        
//        创建导航控制器
        let nav = DLMainNavgationController(rootViewController: childController)
//        添加子控制器
        addChildViewController(nav)
        
        
        
        
        
    }

}
