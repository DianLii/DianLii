//
//  DLMainNavgationController.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/11/30.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit

class DLMainNavgationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.interactivePopGestureRecognizer?.delegate = self
    }
    
    //重写 push
    override func pushViewController(_ viewController: UIViewController, animated: Bool){
        
        print(viewControllers.count)
        // 隐藏 tabbar
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: true)
        print(viewControllers.count)
        if viewControllers.count > 1 {
            if viewControllers.count == 2 {
                //  导航栏中第一个控制器名字
                let title = viewControllers.first!.title!
                
                viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: title, imageName: "navigationbar_back_withtext", target: self, action: #selector(popAction))
            }else{
                //   统一改成但会标题
                viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", imageName: "navigationbar_back_withtext", target: self, action: #selector(popAction))
            }
            
            viewController.title = "第\(viewControllers.count)个控制器"
        }
        
    }
    func popAction() {
        self.popViewController(animated: true)
    }
    
    
    
}

extension DLMainNavgationController: UIGestureRecognizerDelegate{
    //       是否处理这次点击的边缘手势
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        // 如果在第一个视图继续可以拖动会出现bug  
        if viewControllers.count == 1 {
            return false
        }
        return true
    }
    
    
}
