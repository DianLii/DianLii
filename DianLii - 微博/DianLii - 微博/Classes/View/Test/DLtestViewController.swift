//
//  DLtestViewController.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/11/30.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit

class DLtestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setupUI()
    }
    
    
    private func setupUI() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "push", target: self, action: #selector(pushAction))
        
    }
    func pushAction() {
        let testVc = DLtestViewController()
        
        navigationController?.pushViewController(testVc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
