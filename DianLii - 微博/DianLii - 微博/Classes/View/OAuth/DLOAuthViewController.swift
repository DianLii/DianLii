//
//  DLOAuthViewController.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/12/2.
//  Copyright © 2016年 李典. All rights reserved.
//
/*
 https://api.weibo.com/oauth2/authorize?client_id=1351503262&redirect_uri=http://www.jianshu.com
 https://api.weibo.com/oauth2/authorize?client_id=1351503262&redirect_uri=http://www.jianshu.com
 */
import UIKit
import SVProgressHUD

class DLOAuthViewController: UIViewController {
    let AppKey: String = "1351503262"
    let Redirect_uri: String = "http://www.jianshu.com"
    
    private lazy var webViewVc: UIWebView = UIWebView()
    override func loadView() {
        
        webViewVc.delegate = self
        view = webViewVc
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRequest()
        setupNavUI()
        
        
        
    }
    
    
    private func setupNavUI() {
//        解决webView   拖动的时候底部黑条的问题
        webViewVc.isOpaque = false
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "自动填充", target: self, action: #selector(autoFillAction))
            
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", target: self, action: #selector(cancleAvtion))
    }
    //nav的点击事件 自动填充
    @objc private func autoFillAction() {
        // document.getElementById('userId').value = '13836200729'
        //  执行JavaScript
        webViewVc.stringByEvaluatingJavaScript(from: "document.getElementById('userId').value = '13836200729'; document.getElementById('passwd').value = '6864677+'")
        
    }
    @objc private func cancleAvtion() {
        dismiss(animated: true, completion: nil)
        
    }
    
    private func loadRequest() {
        let url = "https://api.weibo.com/oauth2/authorize?client_id=\(AppKey)&redirect_uri=\(Redirect_uri)"
        
        let urlRequest = URLRequest(url: URL(string: url)!)
        
        webViewVc.loadRequest(urlRequest)
    }
    
    

    

}

extension UIViewController: UIWebViewDelegate {
    public func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SVProgressHUD.dismiss()
    }
}

