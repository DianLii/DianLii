//
//  DLMessageTableViewController.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/11/30.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit

class DLMessageTableViewController: DLVisitorTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isLogin == false {
            visitorView?.updateVisiterInfo(image: "visitordiscover_image_message", Info: "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知")
        }
    }
}
