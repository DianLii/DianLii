//
//  DLProfileTableViewController.swift
//  DianLii - 微博
//
//  Created by 李典 on 16/11/30.
//  Copyright © 2016年 李典. All rights reserved.
//

import UIKit

class DLProfileTableViewController: DLVisitorTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isLogin == false{
            visitorView?.updateVisiterInfo(image: "visitordiscover_image_profile", Info: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")
            
        }else{
            setupUI()
        }
        
    }
    private func setupUI() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "push", target: self
            , action: #selector(pushAction))
    }
    
    func pushAction() {
        let testVc = DLtestViewController()
        self.navigationController?.pushViewController(testVc, animated: true)
        
        
    }
}
